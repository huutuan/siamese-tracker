import os
import cv2
import numpy as np
import json
import base64
import time
from handle_track import model

from flask import Flask, render_template, redirect, Response, request, jsonify
app = Flask(__name__)

tracker = model()
bb_init = False


# @app.route('/query_bb')
# def send_bb():
#     r = request
#     return Response(response='hello')


@app.route('/received', methods = ['POST'])
def get_data():
    global tracker, bb_init

    def handle(id, bb):
        return {'id':id, 'bbox': bb}

    if request.method == 'POST':
        content = request.json

        # decode frame
        content = json.loads(content)
        frame_encode = content['frame'].encode('utf-8')    ## encode string to bytes (utf8)
        frame_decode1 = base64.b64decode(frame_encode)      ## decode base64 to bytes
        frame_decode2 = np.fromstring(frame_decode1, np.uint8)      ## decode bytes to np
        _frame_decode = cv2.imdecode(frame_decode2, cv2.IMREAD_COLOR)       ## decode img to (x,x,3)

        _id = content['id']     #id of frame to track
        _bbox = content['bbox']     # bbox for this id

        print('\nid---', _id)
        print('frame shape----', _frame_decode.shape, _frame_decode.dtype)

        # content = np.fromstring(content, np.uint8)
        # print(content.shape)
        # id = content.split('|')[0]
        # img_enc =  content.split('|')[1]
        # bb = tuple(int(r) for r in content.split('|')[2].split(','))
        # print(id, bb)
        # img_enc = np.fromstring(content['frame'], np.uint8)   #string to numpy
        # img_decode = cv2.imdecode(content, cv2.IMREAD_COLOR)  #
        # print('decode shape--',img_decode.shape)
        # cv2.imshow('img decode', img_decode)
        # cv2.waitKey()
        # frames.append(img_decode.shape)

        # start tracker
        print('bb inited ', bb_init)
        if bb_init is False:
            print('init bbox---', _bbox)
            tracker.init(_frame_decode, _bbox)
            bbox_res = _bbox
            bb_init = True
        else:
            Stime = time.time()
            outputs = tracker.track(_frame_decode)
            print('time to track ---', time.time()-Stime)
            bbox_res = list(map(int, outputs['bbox']))
            print('bbox track for id {} ----'.format(_id), bbox_res)

        # return Response(response='ookk', status = 200, mimetype = 'text/plain')
        return jsonify(handle(_id, bbox_res))


if __name__ == '__main__':
    app.run(debug=True, port=5000)

