# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import os
import argparse

import cv2
import torch
import numpy as np
from glob import glob

import time
import sys
import json

# from myapp import SomeObject
sys.path.append("../pysot")
from pysot.core.config import cfg
from pysot.models.model_builder import ModelBuilder
from pysot.tracker.tracker_builder import build_tracker

torch.set_num_threads(8)

print('start-------')
# # siamrpn_alex_dwxcorr
# CONFIG = 'E:\\tuanlh\\pysot\\experiments\\siamrpn_alex_dwxcorr\\config.yaml'
# SNAPSHOT = 'E:\\\\tuanlh\\pysot\\experiments\\siamrpn_alex_dwxcorr\\model.pth'

## siamrpn_mobilev2_l234_dwxcorr
CONFIG = 'E:\\tuanlh\\pysot\\experiments\\siamrpn_mobilev2_l234_dwxcorr\\config.yaml'
SNAPSHOT = 'E:\\tuanlh\\pysot\\experiments\\siamrpn_mobilev2_l234_dwxcorr\\model.pth'

# # siamrpn_r50_l234_dwxcorr
# CONFIG = 'E:\\tuanlh\\pysot\\experiments\\siamrpn_r50_l234_dwxcorr\\config.yaml'
# SNAPSHOT = 'E:\\tuanlh\\pysot\\experiments\\siamrpn_r50_l234_dwxcorr\\model.pth'


# ## siammask_r50_l3
# CONFIG = 'E:\\tuanlh\\pysot\\experiments\\siammask_r50_l3\\config.yaml'
# SNAPSHOT = 'E:\\tuanlh\\pysot\\experiments\\siammask_r50_l3\\model.pth'


def model():
    # load config
    cfg.merge_from_file(CONFIG)
    cfg.CUDA = True
    device = torch.device('cuda')
    print('CUDA ------------------ ', torch.cuda.is_available())
    # arct = torch.load(SNAPSHOT, map_location=lambda storage, loc: storage.cpu()).keys()
    # print(arct)
    # return

    # create model
    model = ModelBuilder()
    # load model
    model.load_state_dict(torch.load(SNAPSHOT,
        map_location=lambda storage, loc: storage.cuda()))
    model.eval().to(device)

    # build tracker
    tracker = build_tracker(model)
    return tracker
