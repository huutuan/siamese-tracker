import requests
import json
import cv2
import time
import numpy as np
import base64
import argparse
parser = argparse.ArgumentParser(description='tracking through server')
parser.add_argument('--video', type=str, help='videos file')
args = parser.parse_args()


content_type = 'application/json'
headers = {'content-type':content_type}


def parse(id, frame, bbox):
    return json.dumps({'id': id, 'frame': frame, 'bbox': bbox})


def get_init_bb(img):
    init_rect = cv2.selectROI('video', img, False, False)
    return init_rect


def draw(frame, bbox, fps):
    position = (50,50)
    font = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 1
    color = (255,0,0)
    thinkness = 2
    cv2.rectangle(frame, (bbox[0], bbox[1]),
                              (bbox[0]+bbox[2], bbox[1]+bbox[3]),
                              (0, 255, 0), 3)
    cv2.putText(frame, str(fps) + ' fps', position, font, fontScale, color, thinkness, cv2.LINE_AA)
    cv2.imshow('video', frame)
    cv2.waitKey(1)
    return fps


def main():
    cap = cv2.VideoCapture(args.video)
    count = 0
    bb = None

    while True:
        Stime0 = time.time()
        ret, frame = cap.read()
        if ret:
            if count == 0:      # init bb with first frame
                bb = get_init_bb(frame)
                print('init bbox----', bb, '---type---', type(bb))
                start_time = time.time()

            _, img_enc = cv2.imencode('.jpg', frame)    ## encode np to bytes
            img_enc = base64.b64encode(img_enc)         ## encode bytes to base64
            im = img_enc.decode('utf-8')                ## decode base64 to string

            data = parse(count+1, im, bb)           # parse to json

            # send request
            print('\nframe {} ---'.format(count+1))
            Stime1 = time.time()
            response = requests.post('http://127.0.0.1:5000/received', json = data, headers=headers)
            print('time request  ----', time.time()-Stime1)

            # get response
            json = response.json()
            bb = json['bbox']
            fps = count / (time.time() - start_time)
            print('bbox response ---', bb)
            print('time from {}  to  {}'.format(start_time, time.time()))

            # draw
            Stime2 = time.time()
            fps = draw(frame, bb, fps)
            print('time to draw cv2 ---', time.time()-Stime2)
            print('fps ---', fps)

            count += 1
        else:
            break
        print('sum of time for frame ---', time.time()-Stime0)

if __name__ == __name__:
    main()